﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using WebApplication1.Service;

namespace WebApplication1.Controllers
{
    [RoutePrefix("BookStore")]
    public class BookStoreAPIController : Controller
    {
        [Route("GetBooks")]
        public ActionResult GetBooks()
        {
            var service = new BookService();

            var books = service.GetBooks();

            return Json(books, JsonRequestBehavior.AllowGet);
        }


        [Route("GetAuthors")]
        public ActionResult GetAuthors()
        {
            var service = new AuthorService();

            var authors = service.GetAuthors();

            return Json(authors, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [Route("GetAuthor")]
        public ActionResult GetAuthor(int id)
        {
            var service = new AuthorService();

            var author = service.GetAuthor(id);

            return Json(author);
        }

        [Route("CreateAuthor")]
        public ActionResult CreateAuthor(string name)
        {
            var service = new AuthorService();

            var author = service.CreateAuthor( name);

            return Json(author, JsonRequestBehavior.AllowGet);
        }

        [Route("UpdateAuthor")]
        public ActionResult UpdateAuthor(int id)
        {
            var service = new AuthorService();

            var author = service.UpdateAuthor(id);


           

            return Json(author, JsonRequestBehavior.AllowGet);
        }

        [Route("DeleteAuthors")]
        public ActionResult DeleteAuthors(int id)
        {
            var service = new AuthorService();

            var author = service.DeleteAuthors(id);




            return Json(author, JsonRequestBehavior.AllowGet);
        }



    }

}


