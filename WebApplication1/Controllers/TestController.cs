﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using WebApplication1.Service;

namespace WebApplication1.Controllers
{
    [RoutePrefix("Test")]
    public class TestController : Controller
    {
        [Route("Text")]
        public ActionResult GetText()
        {
            return this.Content("this is a text", "text/html");
        }

        [Route("Person/List")]
        public ActionResult GetPersons()
        {
            var list = new List<Person>();

            list.Add(new Person { Id = 1, Name = "Dritan" });
            list.Add(new Person { Id = 1, Name = "Elvis" });
            list.Add(new Person { Id = 1, Name = "Angela" });

            return Json(list, JsonRequestBehavior.AllowGet);

        }


        [Route("Book/List")]
        public ActionResult Books() {
            var service = new BookService();

            var books = service.GetBooks();

            ViewBag.Books = books;
            ViewBag.Message = "Hello World!";

            return View();
        }



    }
}