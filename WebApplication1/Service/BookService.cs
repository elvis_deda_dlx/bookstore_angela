﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.Service
{
    public class BookService
    {
        public BookService()
        {

        }

        public IEnumerable<BookDto> GetBooks()
        {
            using (var ctx = new BookStoreEntities())
            {
                var books = ctx.Book;

                var list = books.Select(x => new BookDto { Id = x.BookId, Title = x.Name }).ToList();

                return list;
            }
        }
    }
}