﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.Service
{
    public class AuthorService
    {
        public IEnumerable<AuthorDto> GetAuthors()
        {

            using (var ctx = new BookStoreEntities())
            {                
                var authors = ctx.Author;
                var list = authors.Select(x => new AuthorDto { AuthorId = x.AuthorId, Name = x.Name }).ToList();
                return list;

            }


        }


        public AuthorDto GetAuthor(int id)
        {

            using (var ctx = new BookStoreEntities())
            {


                var authors = ctx.Author;

                var query = from a in ctx.Author
                            where a.AuthorId == id
                            select new AuthorDto { AuthorId = a.AuthorId, Name = a.Name };

                var author = query.FirstOrDefault();



                return author;
            }
        }





        public Author CreateAuthor(string name)
        {

            using (var ctx = new BookStoreEntities())
            {


                var authors = ctx.Author;
                Author a = new Author();



                a.Name = name;
                DateTime date = DateTime.Today;

                a.CreatedDate = date;

                authors.Add(a);
                ctx.SaveChanges();

                return a;

            }


        }

        public Author UpdateAuthor(int id)
        {

            using (var ctx = new BookStoreEntities())
            {


                var authors = ctx.Author;
                var query = (from a in authors
                             where a.AuthorId == id
                             select a);
                var author = query.FirstOrDefault();
                author.Name = "anxhela";

                authors.Add(author);



                ctx.SaveChanges();



                return author;

            }


        }

        public IEnumerable<AuthorDto> DeleteAuthors(int id)
        {

            using (var ctx = new BookStoreEntities())
            {


                var authors = ctx.Author;
                var query = (from a in authors
                             where a.AuthorId == id
                             select a);
                var author = query.FirstOrDefault();

                authors.Remove(author);


                ctx.SaveChanges();
                var list = authors.Select(x => new AuthorDto { AuthorId = x.AuthorId, Name = x.Name }).ToList();

                return list;

            }


        }

    }
}