﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class AuthorDto
    {
        public int AuthorId { get; set; }
        public string Name { get; set; }
        public System.DateTime CreatedDate { get; set;}
           
        public string UpdatedDate { get; set; }

}
}